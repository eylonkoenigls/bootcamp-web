import "./App.css";
// import React, { useState } from "react";
// import { Route, Routes, BrowserRouter } from "react-router-dom";
import Home from "./Pages/Home/Home";
import OpenPosition from "./Components/Open Position/OpenPosition";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import PersonalSpace from "./Pages/PersonalSpace/PersonalSpace";
// import "./App.css";
import UserContext from "./contexts/UserContext";
import { Position } from "./Pages/Position/Position";
import Register from "./Pages/Register/Register";
import Login from "./Pages/Login/Login";
import { useEffect, useState } from "react";
import Header from "./Components/Header/Header";
import Footer from "./Components/Footer/Footer";
import Eula from "./Pages/Eula/Eula";

function App() {
  const [homePSearch, setHomePSearch] = useState([]);
  const [userAccessToken, setUserAccessToken] = useState(null);
  const [isUserConnected, setUserConnected] = useState(false);
  // const isUserConnected = false;
  const [userName, setUserName] = useState(null);
  useEffect(() => {
    const token = localStorage.getItem("Token");
    if (token) {
      setUserAccessToken(token);
    }
  }, []);
  useEffect(() => {
    if (!userAccessToken) {
      setUserConnected(false);
      return;
    }
    setUserConnected(true);
  }, [userAccessToken]);

  return (
    <div className="App">
      <BrowserRouter>
        {/* <SearchHomePageContext.Provider value={{ homePSearch, setHomePSearch }}> */}

        {/* </SearchHomePageContext.Provider> */}
      </BrowserRouter>
      <UserContext.Provider
        value={{
          userAccessToken,
          isUserConnected,
          setUserAccessToken,
          userName,
          setUserName,
          homePSearch,
          setHomePSearch,
        }}
      >
        <BrowserRouter>
          <Header />
          <Routes>
            <Route
              exact
              path="/"
              element={<Navigate to={isUserConnected ? "/home" : "/login"} />}
            />
            <Route
              exact
              path="/login"
              element={isUserConnected ? <Navigate to="/home" /> : <Login />}
            />
            <Route exact path="/register" element={<Register />} />
            <Route
              exact
              path="/home"
              element={isUserConnected ? <Home /> : <Navigate to="/login" />}
            />
            <Route
              exact
              path="/positions/search"
              element={
                isUserConnected ? <OpenPosition /> : <Navigate to="/login" />
              }
            />
            <Route
              exact
              path="/positions/:id"
              element={
                isUserConnected ? <Position /> : <Navigate to="/login" />
              }
            />
            <Route
              exact
              path="/personalSpace"
              element={
                isUserConnected ? <PersonalSpace /> : <Navigate to="/login" />
              }
            />
            <Route
              exact
              path="/eula"
              element={isUserConnected ? <Eula /> : <Navigate to="/login" />}
            />
          </Routes>
          <Footer />
        </BrowserRouter>
      </UserContext.Provider>
    </div>
  );
}

export default App;
