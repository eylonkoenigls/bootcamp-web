import React, { useContext } from "react";
import "./Header.css";
import { ReactComponent as LogoLs } from "../../assets/images/Logo1.5x.svg";
import { Link, useLocation, useNavigate } from "react-router-dom";
import UserContext from "../../contexts/UserContext";

function Header() {
  const { setUserAccessToken } = useContext(UserContext);
  const location = useLocation();
  const navigate = useNavigate();
  const isDefaultPage =
    location.pathname !== "/login" && location.pathname !== "/register";

  const getLinkProperties = (path) => {
    if (path === "/login") return ["/register", "הירשם"];
    else if (path === "/register") return ["/login", "התחבר"];
    else return ["/personalSpace", "כניסה לאזור האישי"];
  };

  const [linkUrl, linkTitle] = getLinkProperties(location.pathname);

  const onClickExit = () => {
    localStorage.removeItem("Token");
    setUserAccessToken(null);
  };

  return (
    <div className="header-container">
      <div className="div-header-start">
        <Link className="link" to={linkUrl}>
          <div className="div-ezor-ichi">{linkTitle}</div>
        </Link>
        {!isDefaultPage && (
          <Link className="link" to="/register">
            <div className="div-ein-hechbon">?אין לך חשבון</div>
          </Link>
        )}
      </div>
      {isDefaultPage ? (
        <>
          <div className="div-hitnatek" onClick={onClickExit}>
            התנתק
          </div>
          <div className="div-header-middle">
            <Link className="link" to="/eula">
              <div className="div-middle-button takanon">תקנון</div>
            </Link>
            <Link className="link" to="/positions/search">
              <div className="div-middle-button misrot">משרות פתוחות</div>{" "}
            </Link>
            <Link className="link" to="/home">
              <div className="div-middle-button bait">בית</div>
            </Link>
          </div>
        </>
      ) : (
        <div className="div-header-middle"></div>
      )}
      <div>
        <div className="div-logo">
          <LogoLs />
        </div>
      </div>
    </div>
  );
}

export default Header;
