import "./PositionComponent.css";
import React, { useContext, useEffect, useState } from "react";
import { AddCVForm } from "../AddCVForm/AddCVForm";
import { useParams } from "react-router-dom";
import axios from "axios";
import UserContext from "../../contexts/UserContext";

export const PositionComponent = ({ title, date, description, id }) => {
  const positionDate = date.substring(0, 10);
  return (
    <div className="position-and-form-container">
      <div className="position-container">
        <div className="position-fields-container">
          <div className="position-title">{title}</div>
          <div className="position-date">{positionDate}</div>
          <div className="position-description">{description}</div>
        </div>
      </div>
      <AddCVForm positionId={id} />
    </div>
  );
};
