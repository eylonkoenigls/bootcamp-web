import React from "react";
import "./Footer.css";

function Footer() {
    return (
        <div className="div-footer-container">
            <div className="div-footer-text">
                Powered by LS Technology LTD | © Copyrights 2022 | Contact Us
            </div>
        </div>
    );
}
export default Footer;