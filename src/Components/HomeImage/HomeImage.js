import React, { useState } from "react";
import "./HomeImage.css";
import { NavLink, useNavigate } from "react-router-dom";

function HomeImage() {
  const [search, setSearch] = useState("");
  const navigate = useNavigate();
  const goToOpenPositions = (event) => {
    if (event.key === "Enter") {
      navigate(`/positions/search?search=${search}`);
    }
  };
  return (
    <div>
      <div className="homePage-Image">
        <div className="homePage-img-title">
          <h1 className="homePage-topHeader">חבר מביא חבר</h1>
          <p className="homePage-smallTitle" id="description">
            !הפכו את הקשרים שלכם לכסף
          </p>
          <div className="homePage-searchInput">
            <NavLink to={`/positions/search?search=${search}`}>
              <img className="searchIconHomePage" src="Search@2x.svg" alt="1" />
            </NavLink>
            <input
              type={"text"}
              className="homePage-searchBox"
              placeholder="חפש משרה ספציפית"
              onChange={({ target: { value } }) => setSearch(value)}
              onKeyDown={(e) => goToOpenPositions(e)}
            />
          </div>
        </div>
        <div className="divIAlsoLookPbutton">

          <button className="IAlsoLookPbutton" onClick={() => navigate(`/positions/search`)}>
            <p className="IAlsoLookPbuttonP">אני גם מחפש</p>
            <img className="IAlsoLookPbuttonImg" src="Content.png" alt="1" />
          </button>
        </div>
      </div>
    </div>
  );
}

export default HomeImage;
