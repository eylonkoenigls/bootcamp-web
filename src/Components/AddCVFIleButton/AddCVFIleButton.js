import { useEffect, useRef, useState } from "react";
import addCVIcon from "../../assets/Apply/add_cv_icon3x.svg";
import "./AddCVFIleButton.css";

export const AddCVFIleButton = ({ setCvFile, cvFile }) => {
  const fileInputRef = useRef(null);
  const [showFile, setShowFile] = useState(false);

  useEffect(() => {
    setShowFile(true);
  }, [cvFile]);

  useEffect(() => {
    setShowFile(false);
  }, []);

  const deleteFile = () => {
    setCvFile("");
    setShowFile(false);
  };

  return (
    <div>
      <div
        className="add-cv-file-button"
        onClick={() => {
          fileInputRef.current.click();
        }}
      >
        <img
          className="add-cv-file-button-img"
          // htmlFor="input-cv-file"
          src={addCVIcon}
          alt="add cv"
        />
        <div className="add-cv-file-button-text">הוסף קובץ קורות חיים</div>

        <input
          ref={fileInputRef}
          type={"file"}
          id="input-cv-file"
          onChange={(e) => {
            if (e.target.files) {
              setCvFile(e.target.files[0]);
            }
          }}
          hidden
        ></input>
      </div>
      <div className="delete-file-button">
        {showFile && (
          <div className="display-file-name">
            {cvFile.name}
            <button
              className="delete-file-name-button clickable"
              onClick={deleteFile}
            >
              x
            </button>
          </div>
        )}
      </div>
    </div>
  );
};
