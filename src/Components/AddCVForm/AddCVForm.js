import React, { useContext, useState } from "react";
import UserContext from "../../contexts/UserContext";
import "./AddCVForm.css";
import { useForm } from "react-hook-form";
import { AddCVFIleButton } from "../AddCVFIleButton/AddCVFIleButton";
import SuggestionAccepted from "../SuggestionAccepted/SuggestionAccepted";
import axios from "axios";

export const AddCVForm = ({ positionId }) => {
  const { userAccessToken } = useContext(UserContext);

  //   "Bearer "+userAccessToken;
  const [newNameInput, setNewNameInput] = useState("");
  const [newPhoneInput, setNewPhoneInput] = useState("");
  const [newYearsInput, setNewYearsInput] = useState("");
  const [cvFile, setCvFile] = useState("");
  const [showPopup, setShowPopup] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  // clear inputs after successful form send
  const deleteInputs = () => {
    setNewNameInput("");
    setNewPhoneInput("");
    setNewYearsInput("");
    setCvFile("");
  };

  // send the form to the server

  const onSubmit = () => {
    postCV();
    deleteInputs();
  };

  const postCV = async () => {
    const data = new FormData();
    data.append("cv", cvFile);
    data.append(
      "cvSubmitted",
      JSON.stringify({
        position: positionId,
        // "61f034e246b27b553f3cc5a7",
        submitedMySelf: false,
        candidate: {
          name: newNameInput,
          phoneNumber: newPhoneInput,
          yearsOfExperience: newYearsInput,
          cvStatus: "in progress",
        },
      })
    );

    try {
      const postReq = await axios.post(
        "http://3.16.162.99/api/cvsubmision/",
        data,
        {
          headers: {
            "content-type": "multipart/form-data",
            authorization: `bearer ${userAccessToken}`,
          },
        }
      );

      const resAlert = postReq.data.message;
      postReq.status === 200 ? setShowPopup(true) : alert(resAlert);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="apply-container apply-container-background">
      {showPopup && <SuggestionAccepted setShow={setShowPopup} />}
      <div className="apply">
        <div className="apply-title">
          <div className="main-apply-title">
            {positionId ? "בשביל מה יש חברים?" : "לא מצאת משרה מתאימה לחבר?"}
          </div>
          <div className="secondary-apply-title">
            {positionId
              ? "אנחנו רגע מלסדר לחבר שלך עבודה"
              : "שלח לנו קורות חיים ואולי אנחנו נמצא עבורו משהו מתאים!"}
          </div>
        </div>
        <h3 className="input-candidate-details">
          {positionId ? "הזן את פרטי המועמד" : ""}
        </h3>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="input-cv-form">
            <div className="input-cv-inputs">
              <div className="error-input"> {errors.name?.message}</div>
              {!errors.name?.message && errors.name && (
                <div className="error-input">שדה זה חובה</div>
              )}
              <div className="input-box-container">
                <input
                  required
                  name="name"
                  type="text"
                  {...register("name", {
                    required: true,
                    pattern: {
                      value: /^[a-zA-Z\s\u0590-\u05fe]+$/i,
                      message: "שדה זה חייב להכיל רק אותיות ",
                    },
                  })}
                  className="input-box"
                  placeholder="שם החבר המועמד"
                  value={newNameInput}
                  onChange={(e) => setNewNameInput(e.target.value)}
                ></input>
              </div>
              <div className="error-input"> {errors.phone?.message}</div>
              {!errors.phone?.message && errors.phone && (
                <div className="error-input">שדה זה חובה</div>
              )}
              <div className="input-box-container">
                <input
                  required
                  name="phone"
                  {...register("phone", {
                    required: true,
                    pattern: {
                      value:
                        /^\+?(972|0)(-)?0?(([23489]{1}\d{7})|[5]{1}\d{8})$/,
                      message: "מספר הטלפון לא תקין",
                    },
                  })}
                  className="input-box"
                  placeholder="מספר טלפון"
                  value={newPhoneInput}
                  onChange={(e) => setNewPhoneInput(e.target.value)}
                ></input>
              </div>
              {errors.years && <div className="error-input">שדה זה חובה</div>}
              <div className="input-box-container">
                <input
                  required
                  name="years"
                  type="number"
                  {...register("years", {
                    required: true,
                    maxLength: 2,
                    minLength: 1,
                  })}
                  className="input-box"
                  placeholder="שנות נסיון"
                  value={newYearsInput}
                  onChange={(e) => setNewYearsInput(Number(e.target.value))}
                ></input>
              </div>

              <div className="clickable">
                <AddCVFIleButton setCvFile={setCvFile} cvFile={cvFile} />
              </div>
            </div>
            <div className="button-icon-primary-enabled clickable">
              <div className="btn-text-container">
                <button id="submit-button" className="clickable" type="submit">
                  {positionId ? "הגש הצעה" : "שלח קורות חיים"}
                </button>
              </div>
            </div>
            {positionId ? (
              ""
            ) : (
              <div className="check-container">
                <input
                  type="checkbox"
                  id="check"
                  className="check-box-eula"
                ></input>
                <label htmlFor="check" className="check-text">
                  אני מאשר/ת כי קראתי תקנון
                </label>
              </div>
            )}
          </div>
        </form>
      </div>
    </div>
  );
};
