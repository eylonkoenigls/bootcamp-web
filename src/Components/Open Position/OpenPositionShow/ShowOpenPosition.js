import React from "react";
import "./ShowOpenPosition.css";
import PositionCard from "./PositionCard/PositionCard";

const ShowOpenPosition = ({ positionsOpen }) => {
  return (
    <section className="cardsOpenP2">
      {positionsOpen.map((pos) => (
        <PositionCard
          Key={pos._id}
          id={pos._id}
          Position={pos.title}
          Date={pos.job_should_start}
          Description={pos.job_description}
        />
      ))}
    </section>
  );
};
export default ShowOpenPosition;
