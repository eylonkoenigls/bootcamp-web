import React from "react";
import { useNavigate } from "react-router-dom";
import "./PositionCard.css";

const PositionCard = ({ id, Position, Date, Description }) => {
  const positionDate = Date?.substring(0, 10);
  const navigate = useNavigate();

  if (Description.length > 250) {
    Description = Description.slice(0, 778) + String("...");
  }
  return (
    <div className="OpenPositionsJob">
      <div className="positionCards1">
        <h3 className="positionH">{Position}</h3>
        <div className="cardsDate positionH">{positionDate}</div>
        <p>{Description}</p>
      </div>
      <div>
        <button
          className="sendCVofFriend"
          id={id}
          onClick={() => navigate(`/positions/${id}`)}
        >
          יש לי חבר מתאים!
        </button>
      </div>
    </div>
  );
};

export default PositionCard;
