import "./OpenPosition.css";
import React, { useState, useRef, useEffect, useContext } from "react";
import axios from "axios";

import "./OpenPosition.css";
import ShowOpenPosition from "./OpenPositionShow/ShowOpenPosition";
import { useSearchParams } from "react-router-dom";
import UserContext from "../../contexts/UserContext";
import Spinner from "../Spinner/Spinner";

const OpenPosition = () => {
  const { userAccessToken } = useContext(UserContext);

  const [positions, setPositions] = useState("");
  const [searchParam, setSearchParam] = useSearchParams();
  const [spinner, setSpinner] = useState(<Spinner />);

  useEffect(() => {
    freeSearch(searchParam.get("search") || "");
  }, [searchParam]);

  async function freeSearch(search) {
    let res = await axios.get(
      `http://3.16.162.99/api/jobSearch/searchFree?word_to_search_free=${search}`,
      {
        headers: {
          "content-type": "application/json",
          authorization: `bearer ${userAccessToken}`,
        },
      }
    );
    let data = await res.data;

    setPositions(data);
    if (data.length === 0 || !data) {
      setSpinner(<h1>no results</h1>);
    }
    if (data.length > 0) {
    }
  }

  const inputOpanRef = useRef(null);
  useEffect(() => {
    inputOpanRef.current.focus();
  });
  return (
    <div className="pageOpenPosition">
      <div className="containerOpenPosition">
        <h3 className="titleOpenPosition">משרות פתוחות</h3>
        <div className="underline"></div>
        <input
          className="searchOpenPosition"
          value={searchParam.get("search") || ""}
          onChange={({ target: { value: search } }) =>
            setSearchParam({ search })
          }
          ref={inputOpanRef}
        />
        <div>
          <div>
            <ul className="breadcrumb">
              <li>
                <a href="/home">בית</a>
              </li>
              <li>
                <a href="#">משרות פתוחות</a>
              </li>
              <li>{searchParam.get("search") || ""}</li>
            </ul>
          </div>
          {positions.length > 0 ? (
            <ShowOpenPosition positionsOpen={positions} />
          ) : (
            spinner
          )}
        </div>
      </div>
    </div>
  );
};

export default OpenPosition;
