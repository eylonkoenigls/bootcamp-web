import close from "../../assets/Close2x.svg";
import CV from "../../assets/Content1.5x.svg";
import "./SuggestionAccepted.css";
import { Link } from "react-router-dom";

const SuggestionAccepted = ({ setShow }) => {
  return (
    <div className="popup">
      <div className="popup-card">
        <div className="close-popup-button">
          <img src={close} alt="close" onClick={() => setShow(false)} />
        </div>
        <div className="text-in-card">
          <div className="popup-title">ההצעה הוגשה!</div>
          <div className="text-freind-find-job">
            כל שנותר הוא לחכות לטלפון <br /> מאתנו שהחבר מצא עבודה
          </div>
          <div className="text-search"> מחפשים גם?</div>
        </div>
        <div className="buttons-popup">
          <Link to="/personalSpace" className="link-to-path">
            <div className="button-2 post-apply-btn">
              <div className="elements-text-center">עבור לאזור האישי</div>
            </div>
          </Link>
          <Link to="/positions/search" className="link-to-path">
            <div className="button-1 post-apply-btn">
              <div className="in-button-container">
                <img className="cv-icon" src={CV} alt="CV" />
                <div className="also-search-text">אני גם מחפש</div>
              </div>
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};
export default SuggestionAccepted;
