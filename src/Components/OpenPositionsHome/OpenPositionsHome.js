import React, { useContext, useEffect, useState } from "react";
import "./OpenPositionsHome.css";
import OpenPositionsCards from "./OpenPositionsCards/OpenPositionsCards";
import { NavLink } from "react-router-dom";
import axios from "axios";
import UserContext from "../../contexts/UserContext";

const OpenPositionsHome = () => {
  const [getSixPositions, setGetSixPositions] = useState([]);
  const { userAccessToken } = useContext(UserContext);

  const postionsData = async () => {
    let res = await axios.get(
      `http://3.16.162.99/api/jobSearch/displayToHomePage`,
      {
        headers: {
          "content-type": "application/json",
          authorization: `bearer ${userAccessToken}`,
        },
      }
    );

    let data = await res.data;
    setGetSixPositions(data);
  };

  useEffect(() => {
    postionsData();
  }, []);

  return (
    <div className="containerHomePositions">
      <h2 className="openPositionsLine">משרות פתוחות</h2>
      <div className="underlinOpenPositions"></div>
      <OpenPositionsCards postionsData={getSixPositions} />
      <div className="showOtherPositions">
        <NavLink to="/positions/search">
          <button className="showOtherPositionsButton">הצג משרות נוספות</button>
        </NavLink>
      </div>
    </div>
  );
};

export default OpenPositionsHome;
