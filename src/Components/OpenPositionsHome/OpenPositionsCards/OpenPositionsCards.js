import React from "react";
import "./OpenPositionsCards.css";
import HomeCards from "./HomeCards/HomeCards";

const OpenPositionsCards = ({ postionsData, dontCut }) => {
  return (
    <section className="cardsOpenP2">
      {postionsData.map((pos) => (
        <HomeCards
          Key={pos._id}
          id={pos._id}
          Position={pos.title}
          Date={pos.job_should_start}
          Description={pos.job_description}
        />
        // />
      ))}
    </section>
  );
};
export default OpenPositionsCards;
