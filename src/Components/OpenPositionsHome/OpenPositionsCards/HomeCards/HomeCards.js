import React from "react";
import "./HomeCards.css";
import { useNavigate } from "react-router-dom";

const HomeCards = ({ id, Position, Date, Description, dontCut }) => {
  if (!dontCut && Description.length > 50) {
    Description = Description.slice(0, 200) + String("...");
  }
  const positionDate = Date.substring(0, 10);
  const navigate = useNavigate();

  return (
    <div className="OpenPositionsCards">
      <div className="positionDetailsCards">
        <h3 className="positionH">{Position}</h3>
        <div className="cardsDate positionH">{positionDate}</div>
        <p>{Description}</p>
      </div>
      <div>
        <button
          className="IHaveFriendButton"
          onClick={() => navigate(`/positions/${id}`)}
        >
          יש לי חבר מתאים!
        </button>
      </div>
    </div>
  );
};

export default HomeCards;
