import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./Submiting.css";
import UserContext from "../../contexts/UserContext";
import { useForm } from "react-hook-form";
import { AddCVFIleButton } from "../AddCVFIleButton/AddCVFIleButton";
import SuggestionAccepted from "../SuggestionAccepted/SuggestionAccepted";
import axios from "axios";

const Submiting = ({ positionId, newJob }) => {
  const { userAccessToken } = useContext(UserContext);

  //   "Bearer "+userAccessToken;
  const [newNameInput, setNewNameInput] = useState("");
  const [newPhoneInput, setNewPhoneInput] = useState("");
  const [newYearsInput, setNewYearsInput] = useState("");
  const [cvFile, setCvFile] = useState("");
  const [showPopup, setShowPopup] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  // clear inputs after successful form send
  const deleteInputs = () => {
    setNewNameInput("");
    setNewPhoneInput("");
    setNewYearsInput("");
    setCvFile("");
  };

  // send the form to the server

  const onSubmit = () => {
    setShowPopup(true);

    postCV();
    deleteInputs();
  };

  const postCV = async () => {
    const data = new FormData();
    data.append("cv", cvFile);
    data.append(
      "cvSubmitted",
      JSON.stringify({
        position: null,
        submitedMySelf: false,
        candidate: {
          name: newNameInput,
          phoneNumber: newPhoneInput,
          yearsOfExperience: newYearsInput,
          cvStatus: "in progress",
        },
      })
    );

    try {
      const postReq = await axios.post(
        "http://3.16.162.99/api/cvsubmision/",
        data,
        {
          headers: {
            "content-type": "multipart/form-data",
            authorization: `bearer ${userAccessToken}`,
          },
        }
      );
      console.log(postReq.data);
    } catch (e) {
      console.log("Error");
    }
  };

  return (
    <div className="apply-container1 apply-container-background1">
      {/* {showPopup && <SuggestionAccepted setShow={setShowPopup} />} */}
      {showPopup && <SuggestionAccepted setShow={setShowPopup} />}
      <div className="imgComuter-home-container">
        <img className="computerImg" src="/image.png" alt="hi" />
      </div>
      <div className="apply1">
        <div className="apply-title1">
          <div className="main-apply-title1">
            {positionId ? "בשביל מה יש חברים?" : "לא מצאת משרה מתאימה לחבר?"}
          </div>
          <div className="secondary-apply-title1">
            {positionId
              ? "אנחנו רגע מלסדר לחבר שלך עבודה"
              : "שלח לנו קורות חיים ואולי אנחנו נמצא עבורו משהו מתאים!"}
          </div>
        </div>
        <h3 className="input-candidate-details1">
          {positionId ? "הזן את פרטי המועמד" : ""}
        </h3>

        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="input-cv-form1">
            <div className="input-cv-inputs1">
              <div className="error-input1"> {errors.name?.message}</div>
              {!errors.name?.message && errors.name && (
                <div className="error-input1">שדה זה חובה</div>
              )}
              <div className="input-box-container1">
                <input
                  name="name"
                  type="text"
                  {...register("name", {
                    required: true,
                    pattern: {
                      value: /^[a-zA-Z\s\u0590-\u05fe]+$/i,
                      message: "שדה זה חייב להכיל רק אותיות ",
                    },
                  })}
                  className="input-box"
                  placeholder="שם מלא"
                  value={newNameInput}
                  onChange={(e) => setNewNameInput(e.target.value)}
                ></input>
              </div>
              <div className="error-input1"> {errors.phone?.message}</div>
              {!errors.phone?.message && errors.phone && (
                <div className="error-input1">שדה זה חובה</div>
              )}
              <div className="input-box-container1">
                <input
                  name="phone"
                  {...register("phone", {
                    required: true,
                    pattern: {
                      value:
                        /^\+?(972|0)(-)?0?(([23489]{1}\d{7})|[5]{1}\d{8})$/,
                      message: "מספר הטלפון לא תקין",
                    },
                  })}
                  className="input-box"
                  placeholder="מספר טלפון"
                  value={newPhoneInput}
                  onChange={(e) => setNewPhoneInput(e.target.value)}
                ></input>
              </div>
              {errors.years && <div className="error-input1">שדה זה חובה</div>}
              <div className="input-box-container1">
                <input
                  name="years"
                  type="number"
                  {...register("years", {
                    required: true,
                    maxLength: 2,
                    minLength: 1,
                  })}
                  className="input-box"
                  placeholder="שנות נסיון"
                  value={newYearsInput}
                  onChange={(e) => setNewYearsInput(Number(e.target.value))}
                ></input>
              </div>

              <div className="clickable1">
                <AddCVFIleButton setCvFile={setCvFile} cvFile={cvFile} />
              </div>
            </div>
            <div className="button-icon-primary-enabled1 clickable1">
              <div className="btn-text-container1">
                <button id="submit-button1" type="submit">
                  {positionId ? "הגש הצעה" : "שלח קורות חיים"}
                </button>
              </div>
            </div>
            {positionId ? (
              ""
            ) : (
              <div className="Agree">
                <input
                  type="checkbox"
                  id="checkBox"
                  name="checkBox"
                  value="agree"
                  required
                />
                <label htmlFor="checkBox"> אני מאשר/ת כי קראתי תקנון</label>
              </div>
            )}
          </div>
        </form>
      </div>
    </div>
  );
};
export default Submiting;
