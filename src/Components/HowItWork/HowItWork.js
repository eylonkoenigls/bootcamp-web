import { useRef, useEffect } from "react";
import "./HowItWork.css";

const HowItWork = ({ HandleScroll }) => {
  return (
    <div>
      <div className="howItWorks-home-container">
        <div className="howItWorks-home-container2">
          <div className="howItWorks-home-mainTitle">
            <h3 className="howItWorks-home-Title">איך זה עובד?</h3>
            <div className="underline-howItWorks"></div>
          </div>
          <div className="howItWorks-home-treeSteps-container">
            <div className="howItWorks-home-step1">
              <img src="/1.PNG" alt="1" className="howItWorks-light-img" />
              <div className="howItWorks-home-steps-titleContainer">
                <h5 className="howItWorks-home-titles"> 1. מכירים?</h5>
                <p className="title1 des">
                  חבר שמחפש עבודה? בואו לעשות מזה כסף!
                </p>
              </div>
            </div>
            <div className="group-6">
              <img src="/Group 2.png" alt="2" className="img2" />
              <div className="howItWorks-home-steps-titleContainer">
                <h5 className="howItWorks-home-titles"> 2. מעבירים</h5>
                <p className="title2req des">
                  מבקשים מהחבר קו"ח ומעבירים לנו כאן באתר
                </p>
              </div>
            </div>

            <div className="group-8">
              <img src="/Group 7.png" alt="3" className="img3" />
              <div className="howItWorks-home-steps-titleContainer">
                <h5 className="howItWorks-home-titles"> 3. מרוויחים</h5>
                <p className="title3 des">
                  החבר התקבל לעבודה? אתם מרויחים! צ'ק על סך{" "}
                  <strong>2500-4000₪</strong> , יחכה לכם אצלינו במשרד
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mask">
        <h3 className="submmitFriend-CV">להגשת קו"ח של החבר</h3>
        <button className="submmitFriend-CV-btn" onClick={HandleScroll}>
          לחץ כאן
        </button>
      </div>
    </div>
  );
};

export default HowItWork;
