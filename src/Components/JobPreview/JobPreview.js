import "./JobPreview.css";
import { useNavigate } from "react-router-dom";


function JobPreview(props) {
  let navigate = useNavigate()
  let classer = props.status
  if (props.status === "in process"
  ) {
    classer = "pending";
  }

  let comp = props.company.split(".")

  function addCV() {
    window.location.href = props.Cv
  }

  return (
    <div className="JobPreviewContainer">
      <div className="companyContainer">
        <div className="logoContainer">
          <div className="logo" onClick={addCV}></div>
        </div>
        <div className="companyLine">{comp[0]}</div>
      </div>
      <div> תאריך הגשה: {props.date}</div>
      <div className="applicantDirection">תפקיד: {props.position}</div>
      <div className="applicantDirection"> מועמד: {props.applicant}   </div>
      <div className="statusContainer">
        <div className={classer}>{props.status}</div>
        <div className="statusDirection"> :סטטוס </div>
      </div>
    </div>
  );
}

export default JobPreview;
