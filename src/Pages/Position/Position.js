import React, { useContext, useEffect, useState } from "react";
import "./Position.css";

import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import { PositionComponent } from "../../Components/PositionComponent/PositionComponent";
import { useParams } from "react-router-dom";
import UserContext from "../../contexts/UserContext";
import axios from "axios";
import Spinner from "../../Components/Spinner/Spinner";

export const Position = () => {
  const { userAccessToken } = useContext(UserContext);
  const [description, setDescription] = useState("");
  const [date, setDate] = useState("");
  const [title, setTitle] = useState("");
  const [errMsg, setErrMsg] = useState("loading position");

  async function getPosition(id) {
    // const auth = "Bearer " + userAccessToken;
    let res = null;
    try {
      res = await axios.get(`http://3.16.162.99/api/position/id/${id}`, {
        headers: {
          "content-type": "application/json",
          authorization: `bearer ${userAccessToken}`,
        },
      });
    } catch (e) {
      setErrMsg(e.toString());
      return;
    }
    if (res.status !== 200) {
      setErrMsg("failed fetching position details");
    }
    const data = await res.data;
    if (data[0].job_should_start && data[0].title && data[0].job_description) {
      setDate(data[0].job_should_start);
      setTitle(data[0].title);
      setDescription(data[0].job_description);
    } else {
      setErrMsg("position not found");
    }
  }

  const { id } = useParams();

  useEffect(() => {
    getPosition(id);
  }, []);
  return (
    <>
      {title ? (
        <PositionComponent
          description={description}
          title={title}
          date={date}
          id={id}
        />
      ) : (
        <Spinner />
      )}
    </>
  );
};
