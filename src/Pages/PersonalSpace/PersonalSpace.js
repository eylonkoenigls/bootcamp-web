import "./PersonalSpace.css";
import JobPreview from "../../Components/JobPreview/JobPreview";
import image2 from "./image2.png";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { HashLink } from "react-router-hash-link";
import Spinner from "../../Components/Spinner/Spinner"

function PersonalSpace() {
  const navigate = useNavigate();
  let token = localStorage.getItem("Token");

  const [sliceNum, setSliceNum] = useState(3);
  const [showMoreDisplay, setShowMoreDisplay] = useState("הצג יותר");
  const [scaleDisplay, setScaleDisplay] = useState(false);
  const [dataArray, setDataArray] = useState(false);
  const [miniClass, setMiniClass] = useState("miniJobContainer");
  const [serverguy, setServerGuy] = useState(<Spinner />);
  const [serverguy2, setServerGuy2] = useState(<Spinner />);


  useEffect(() => {
    fetch(`http://3.16.162.99/api/cvsubmision/user`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {

        if (data.length === 0) {
          (setServerGuy(<div className="noJob ">
            <div className="noJobHeadline">
              {" "}
              אנחנו נמצא
              <strong className="strongText">עבור חברך</strong>
              <br></br>
              משהו מתאים{" "}
            </div>
            <HashLink
              to="/Home#checkBox"
              style={{ textDecoration: "none" }}
            >
              <div className="sendCVButton">שלח קו"ח</div>
            </HashLink>
          </div>));
          setServerGuy2(
            <div className="noJob noJobPersonal ">
              <div className="noJobHeadline">
                {" "}
                אנחנו נמצא
                <strong className="strongText">עבורך</strong>
                <br></br>
                משהו מתאים{" "}
              </div>
              <div className="sendCVButton" onClick={sendPersonalCv}>
                שלח קו"ח
              </div>
            </div>)
        }







        if (data.length > 0) {
          const tempArray = data.filter((user) => user.submitedMySelf === true);
          if (tempArray.length === 1) {
            setMiniClass("onePersonal");
          }

          const nullChecker = data.map((v) => {
            if (v.position === null) {
              v.position = {
                title: "משרה כללית",
                job_description: "משרה כללית",
              };
            }
            return v;
          });
          setDataArray(nullChecker);

        }
      });
  }, []);

  function sendPersonalCv() {
    navigate("/positions/search");
  }

  function showDisplay() {
    if (scaleDisplay) {
      setSliceNum(3);
      setShowMoreDisplay("הצג יותר");
      setScaleDisplay(!scaleDisplay);
    } else {
      setSliceNum(60);
      setShowMoreDisplay("הצג פחות");
      setScaleDisplay(!scaleDisplay);
    }
  }


  return (
    <>

      <div className="personalContainer">
        <div className="headline">אזור אישי</div>
        <div className="line"></div>
        <div className="displayContainer">
          <div className="friendHeadline">משרות של חברים</div>
          <div className="friendJobContainer">
            {dataArray ? (
              dataArray
                .filter((user) => user.submitedMySelf === false)
                .slice(0, sliceNum)
                .map((friendArray) => (
                  <JobPreview
                    key={Math.random()}
                    company={friendArray.position.job_description}
                    date={friendArray.createdAt.substring(0, 10)}
                    position={friendArray.position.title}
                    applicant={friendArray.candidate.name}
                    status={friendArray.candidate.cvStatus}
                    Cv={friendArray.candidate.fileURL}
                  />
                ))
            ) : (serverguy)}
          </div>
          {dataArray ? (
            <div className="showMore" onClick={showDisplay}>
              {showMoreDisplay}
            </div>
          ) : (
            <div></div>
          )}

          <div className="myJobHeadline">משרות שלי</div>

          <div className="myJobContainer">
            <img className="folderImage" src={image2} />
            <div className={miniClass}>
              {dataArray ? (
                dataArray
                  .filter((user) => user.submitedMySelf === true)
                  .map((personalArray) => (
                    <JobPreview
                      key={Math.random()}
                      company={personalArray.position.job_description}
                      date={personalArray.createdAt.substring(0, 10)}
                      position={personalArray.position.title}
                      applicant={personalArray.candidate.name}
                      status={personalArray.candidate.cvStatus}
                      Cv={personalArray.candidate.fileURL}

                    />
                  ))
              ) : (serverguy2
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default PersonalSpace;
