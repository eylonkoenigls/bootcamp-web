import "./Eula.css";

const Eula = () => {
  return (
    <div className="eula-lorem-ipsum">
      Exercitation eu officia velit sit excepteur. Ut cupidatat consectetur anim
      enim et laborum eiusmod aliquip ex cillum cillum occaecat. Anim cupidatat
      et magna exercitation. Ea sunt sit labore fugiat proident laboris
      consectetur incididunt adipisicing do nisi. Sint esse laborum dolore duis
      excepteur fugiat nisi irure. Reprehenderit nisi excepteur laborum proident
      nulla laborum aute non sit. Elit dolore ex ex deserunt ad cillum deserunt
      sint mollit. Aliquip nostrud sint dolor aliquip minim non voluptate enim
      dolor qui nostrud. Sunt pariatur et officia anim dolor ex adipisicing
      consectetur velit consequat incididunt tempor. Minim veniam est nostrud
      nisi adipisicing pariatur dolore et irure id pariatur. Magna velit tempor
      cillum voluptate non amet eiusmod fugiat. Eu consectetur officia elit
      veniam dolore. Mollit adipisicing anim anim nostrud. Aute aute non ipsum
      sit commodo occaecat eiusmod veniam ut minim. Tempor nulla cupidatat velit
      fugiat qui nostrud. Incididunt eu eiusmod ad pariatur incididunt dolore ex
      sit adipisicing duis. Exercitation sit mollit incididunt mollit aute magna
      ullamco velit officia irure. Qui officia aliqua occaecat ut sit id duis
      adipisicing anim. Lorem sunt esse duis anim aliqua anim dolore voluptate
      laborum nisi sunt exercitation. Mollit quis officia quis Lorem voluptate.
      Mollit aliquip enim laborum deserunt deserunt elit mollit. Ea exercitation
      nulla enim nostrud ex adipisicing aliqua eiusmod elit cillum velit. Cillum
      ipsum dolor non ex mollit laboris aliqua id occaecat nostrud do nostrud.
      Aute sint occaecat proident proident fugiat anim velit nulla quis
      voluptate. Ullamco sunt voluptate id commodo in irure exercitation
      consectetur ipsum sit deserunt. Minim tempor minim officia enim esse
      incididunt velit enim culpa et non sint qui. Ipsum irure consequat velit
      cupidatat. Sit sunt dolor ullamco ut deserunt. Fugiat consectetur pariatur
      amet officia excepteur. Quis esse culpa non officia quis aute
      reprehenderit ipsum nostrud labore laboris ullamco deserunt enim.
    </div>
  );
};

export default Eula;
