import React, { useContext, useState } from "react";
import "./Register.css";
import VisibilityIcon from "@mui/icons-material/Visibility";
import img from "../../assets/images/imageLogin.png";
import { useForm } from "react-hook-form";
import axios from "axios";
import UserContext from "../../contexts/UserContext";

import { useNavigate } from "react-router-dom";

export default function Rgister() {
  const [passwordShown, setPasswordShown] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [ConfirmPasswordShown, setConfirmPasswordShown] = useState(false);
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    getValues,
    reset,
    formState: { errors },
  } = useForm();

  const { setUserAccessToken } = useContext(UserContext);

  const onSubmit = async () => {
    if (getValues("ConfirmPassword") !== getValues("password")) {
      !passwordError && setPasswordError(true);
      return;
    }
    setPasswordError(false);
    const data = {
      username: getValues("username"),
      password: getValues("password"),
      phone: getValues("phone"),
    };

    axios
      .post("http://3.16.162.99/api/userauth/register", data)
      .then((res) => {
        if (res.status === 200) {
          axios
            .post("http://3.16.162.99/api/userauth/login", {
              username: data.username,
              password: data.password,
            })
            .then((res) => {
              localStorage.setItem("Token", res.data.token);

              setUserAccessToken(res.data.token);
              if (res.data.token) {
                return navigate("/home");
              }
            });
        }
      })
      .catch((err) => {
        console.log(err);
      });
    reset();
  };
  return (
    <>
      <div className="container-register">
        <div className="half-page-img-div-register">
          <img id="img-background-page-register" src={img} alt="img" />
        </div>
        <div className="background-page-40-register">
          <form
            className="form-page-register"
            onSubmit={handleSubmit(onSubmit)}
          >
            <h1 className="header-wellcome-register-ye">ברוכים הבאים</h1>
            <h3 className="header-details-register">הזינו פרטים לכניסה לאתר</h3>
            <div className="inputs-register-page">
              <input
                {...register("username", {
                  required: true,
                  minLength: 2,
                  maxLength: 10,
                })}
                type="text"
                name="username"
                placeholder="*שם משתמש"
                className="userInput-register-page"
              />
              {errors.username && (
                <div className="error-invalid-register-name">
                  שדה זה חובה או שם המשתמש אינו תקין
                </div>
              )}
              <div className="input-div-register-page">
                <VisibilityIcon
                  id="icon-pass"
                  onClick={() => setPasswordShown(!passwordShown)}
                />
                <input
                  {...register("password", {
                    required: true,
                  })}
                  type={passwordShown ? "text" : "password"}
                  name="password"
                  placeholder="*סיסמה "
                  className="pass-value-register"
                />
              </div>

              {errors.password && (
                <div className="error-invalid-register">שדה זה חובה</div>
              )}

              <div className="input-div-register-page">
                <VisibilityIcon
                  id="icon-pass-con"
                  onClick={() => setConfirmPasswordShown(!ConfirmPasswordShown)}
                />
                <input
                  {...register("ConfirmPassword", {
                    required: true,
                  })}
                  type={ConfirmPasswordShown ? "text" : "password"}
                  name="ConfirmPassword"
                  placeholder="*אימות סיסמה"
                  className="pass-value-register"
                />
              </div>

              {errors.ConfirmPassword && (
                <div className="error-invalid-register">שדה זה חובה</div>
              )}
              {!errors.ConfirmPassword && passwordError && (
                <div className="error-invalid-register">
                  סיסמאות אינן תואמות
                </div>
              )}

              <input
                id="type-number"
                {...register("phone", {
                  required: true,
                  pattern: {
                    value: /^\+?(972|0)(-)?0?(([23489]{1}\d{7})|[5]{1}\d{8})$/,
                    message: " מספר נייד אינו תקין",
                  },
                })}
                type="number"
                name="phone"
                placeholder="*טלפון"
                className="userInput-register-page"
              />
              <div className="error-invalid-register">
                {errors.phone?.message}
              </div>
              {!errors.phone?.message && errors.phone && (
                <div className="error-invalid-register">שדה זה חובה </div>
              )}
            </div>
            <button id="button-form-submit-register" type="submit">
              הירשם
            </button>
            <h5 className="header-or-register">או הירשם באמצעות</h5>
            <div className="btn-google-register-form">גוגל</div>
          </form>
        </div>
      </div>
    </>
  );
}
