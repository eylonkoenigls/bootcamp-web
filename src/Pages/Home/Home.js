import "./Home.css";
import Submiting from "../../Components/SubmitingResume/Submiting";
import HomeImage from "../../Components/HomeImage/HomeImage";
import HowItWork from "../../Components/HowItWork/HowItWork";
import OpenPositionsHome from "../../Components/OpenPositionsHome/OpenPositionsHome";
import { useRef } from "react";

function Home() {
  const newJob = useRef(null);
  function HandleScroll() {
    newJob.current.scrollIntoView({ behavior: "smooth" });
  }

  return (
    <>
      <div>
        <HomeImage />
        <HowItWork HandleScroll={HandleScroll} />
        <OpenPositionsHome />
        <Submiting newJob={newJob} />
      </div>
    </>
  );
}

export default Home;
