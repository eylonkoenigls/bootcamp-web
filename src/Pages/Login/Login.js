import React, { useContext } from "react";

import "./Login.css";
import img from "../../assets/images/imageLogin.png";
import { useForm } from "react-hook-form";
import axios from "axios";

import UserContext from "../../contexts/UserContext";
import { useNavigate } from "react-router-dom";

export default function Login() {
  const { setUserAccessToken } = useContext(UserContext);
  const {
    register,
    handleSubmit,
    getValues,
    reset,
    formState: { errors },
  } = useForm();
  const navigate1 = useNavigate();

  const onSubmit = () => {
    const data = {
      username: getValues("username"),
      password: getValues("password"),
    };

    axios
      .post("http://3.16.162.99/api/userauth/login", data)
      .then((res) => {
        localStorage.setItem("Token", res.data.token);

        setUserAccessToken(res.data.token);
        if (res.data.token) {
          return navigate1("/home");
        }
      })
      .catch((err) => {
        console.log(err);
      });
    reset();
  };

  return (
    <>
      <div className="container-login-web">
        <div className="half-page-img-div">
          <img id="img-background-page" src={img} alt="img" />
        </div>

        <div className="half-page-form-div">
          <form
            className="half-page-main-form"
            onSubmit={handleSubmit(onSubmit)}
          >
            <h1 className="header-wellcome-big">ברוכים הבאים</h1>
            <h3 className="header-details-login-site">
              הזינו פרטים לכניסה לאתר
            </h3>
            <div className="middle-end-form-login">
              <div className="inputs-form-login">
                <input
                  {...register("username", {
                    required: true,
                    minLength: 2,
                    maxLength: 10,
                  })}
                  type="text"
                  name="username"
                  placeholder="*שם משתמש"
                  className="userInput-login-form"
                />
                {errors.username && (
                  <div className="error-invalid-value">
                    {" "}
                    שדה זה חובה או שם המשתמש אינו תקין
                  </div>
                )}

                <input
                  {...register("password", {
                    required: true,
                    minLength: 4,
                    maxLength: 10,
                  })}
                  type="password"
                  name="password"
                  placeholder="*סיסמה "
                  className="userInput-login-form"
                />
                {errors.password && (
                  <div className="error-invalid-value">
                    שדה זה חובה או סיסמה אינה תקינה
                  </div>
                )}
              </div>
              <div className="end-form-login-si">
                <button className="button-login-server" type="submit">
                  התחבר
                </button>
                <div className="questions-re-for">
                  <div className="rememberme-login">זכור אותי ממחשב זה</div>
                  <div className="forget-password-login">שכחתי סיסמה</div>
                </div>
                <div className="div-connect-google-login">
                  <h5 className="header-h5-connect-login">או, התחבר באמצעות</h5>
                  <div className="btn-google-login">גוגל</div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
